package chess;
import java.util.Scanner;
public class Chess {
    boolean isGameOver=false;
    Board ChessBoard = new Board();
    int turn=1,srcx,srcy,desx,desy;
    String dx,dy;
    Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        // TODO code application logic here
        Chess myChess = new Chess();
        myChess.Logic();
        
    }
    
   
    public void Logic()
    {
        ChessBoard.printBoard();
        while(!isGameOver)
        {
            TakeInput();
            checkIfKingKilled();
            ChessBoard.CP[srcx][srcy].move(ChessBoard.CP, srcx, srcy, desx, desy);
            checkPawnPromotion();
            ChessBoard.printBoard();
            if(isGameOver)
            {
                System.out.println("Player " + turn + " wins.");
            }
            turn = ((turn == 1)? 2 : 1);
           
            
            
        }
    }
    public boolean checkSourceInput()
    {
        if(srcx< 1 || srcx>8 ||srcy< 1 || srcy>8)
            {
                System.out.println("Wrong Input!!! Enter again");
                System.out.println("Player " + turn + ":");
                System.out.print("Enter Source Coordinates:");
                return false;
            }
        if (ChessBoard.CP[srcx][srcy] == null)
        {
            System.out.println("No piece at given coordinates!!! Enter Again");
            System.out.println("Player " + turn + ":");
            return false;
        }
        if (ChessBoard.CP[srcx][srcy].color!=turn-1)
        {
            System.out.println("Choose your own piece!!!Enter Again");
            System.out.println("Player " + turn + ":");
            return false;
        }
        return true;
    }
    public boolean checkDestinationInput()
    {
        if(desx< 1 || desx>8 ||desy< 1 || desy>8)
            {
                System.out.println("Wrong Input!!! Enter again");
                System.out.println("Player " + turn + ":");
                return false;
            }

        if(!ChessBoard.CP[srcx][srcy].isMoveLegal(ChessBoard.CP,srcx, srcy,desx,desy))
            {
                System.out.println("Move is not legal!!! Enter Again");
                System.out.println("Player " + turn + ":");
                return false;
            }
        return true;
    }

    private void checkIfKingKilled() {
        if(ChessBoard.CP[desx][desy] instanceof King){
            isGameOver = true;
        }
    }

    private void checkPawnPromotion() {
        if(ChessBoard.CP[desx][desy] instanceof Pawn){
            if(desx==1 || desx==8){
                String newPiece= " ";
                while(!newPiece.equals("Q") && !newPiece.equals("K") && !newPiece.equals("R") && !newPiece.equals("B")){
                System.out.println("Pawn is promoted. Enter \n1.'Q' for Queen"
                        + "\n2.'K' for Knight"
                        + "\n3.'R' for Rook"
                        + "\n4.'B' for Bishop ");
                newPiece = sc.next();
                }
                if(newPiece.equals("Q")){
                    ChessBoard.CP[desx][desy] = new Queen("Q"+turn,turn-1);
                }
                else if(newPiece.equals("K")){
                    ChessBoard.CP[desx][desy] = new Knight("N"+turn,turn-1);
                }
                else if(newPiece.equals("R")){
                    ChessBoard.CP[desx][desy] = new Rook("R"+turn,turn-1);
                }
                else if(newPiece.equals("B")){
                    ChessBoard.CP[desx][desy] = new Bishop("B"+turn,turn-1);
                }
                
            }
        }
    }

    private void TakeInput() {
        System.out.println("Player " + turn + ":");
        System.out.print("Enter Source Coordinates:");
        srcx = sc.nextInt();
        srcy = sc.nextInt();
        while(!checkSourceInput()){
            System.out.print("Enter Source Coordinates:");
            srcx = sc.nextInt();
            srcy = sc.nextInt();
        }
        System.out.print("Enter Destination Coordinates(r or R to retake inputs):");
            dx = sc.next();
            if(dx.equals("r") || dx.equals("R"))
                TakeInput();
            else{
                dy = sc.next();
                desx = Integer.parseInt(dx);
                desy = Integer.parseInt(dy);
                while(!checkDestinationInput()){
                    System.out.print("Enter Destination Coordinates(r or R to retake inputs):");
                     dx = sc.next();
                     if(dx.equals("r") || dx.equals("R"))
                        TakeInput();
                     else{
                         dy = sc.next();
                         desx = Integer.parseInt(dx);
                         desy = Integer.parseInt(dy);
                     }
            }
        }
        
    }
}
