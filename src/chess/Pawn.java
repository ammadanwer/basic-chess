package chess;

public class Pawn extends ChessPiece{
    private boolean firstMove=true;
    public Pawn(String ini, int color)
    {
        this.initial = ini;
        this.color = color;
    }

    @Override
    public boolean isMoveLegal(ChessPiece CP[][],int srcx, int srcy, int desx, int desy) {
        if(this.color==0)
        {
            if(firstMove==true && (desx==srcx+1 || desx==srcx+2) && srcy==desy && CP[desx][desy]==null)
            {
                return true;
            }
            else if(firstMove==false && (desx==srcx+1) && srcy==desy && CP[desx][desy]==null)
            {
                return true;
            }
            else if(desx==srcx+1 && (desy == srcy+1 || desy == srcy-1))
            {
                if(CP[desx][desy]==null)
                {
                    return false;
                }
                else if(CP[srcx][srcy].color!=CP[desx][desy].color)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
            else
            {
                return false;
            }
            
        }
        else
        {
            if(firstMove==true && (desx==srcx-1 || desx==srcx-2) && srcy==desy && CP[desx][desy]==null)
            {
                return true;
            }
            else if(firstMove==false && (desx==srcx-1) && srcy==desy && CP[desx][desy]==null)
            {
                return true;
            }
            else if(desx==srcx-1 && (desy == srcy+1 || desy == srcy-1))
            {
                if(CP[desx][desy]==null)
                {
                    return false;
                }
                else if(CP[srcx][srcy].color!=CP[desx][desy].color)
                {
                    return true;
                }
                else{
                    return false;
                }
            }
            else
            {
                return false;
            }
            
        }
    }

    @Override
    public void move(ChessPiece[][] CP, int srcx, int srcy, int desx, int desy) {
        super.move(CP, srcx, srcy, desx, desy);
        firstMove=false;
    }
  
    
}
