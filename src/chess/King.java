package chess;

public class King extends ChessPiece{
    private boolean firstMove=true; 
    public King(String ini, int color)
    {
        this.initial = ini;
        this.color = color;
    }

    @Override
    public boolean isMoveLegal(ChessPiece[][] CP, int srcx, int srcy, int desx, int desy) {
        boolean ans=false;
        if(desx == srcx+1 && (desy == srcy-1 || desy == srcy+1 || desy==srcy))
        {
            ans = perfromChecks(CP, srcx, srcy, desx, desy);
          
        }
        else if(desx == srcx && (desy == srcy-1 || desy == srcy+1))
        {
            ans = perfromChecks(CP, srcx, srcy, desx, desy);

        }
        else if(desx == srcx-1 && (desy == srcy-1 || desy == srcy+1 || desy==srcy))
        {
            ans = perfromChecks(CP, srcx, srcy, desx, desy);
        }
        return ans;
    }
       
    private boolean perfromChecks(ChessPiece[][] CP, int srcx, int srcy, int desx, int desy)
    {
        if(CP[desx][desy]== null){
                return true;
            }
            else{
                if(CP[desx][desy].color == CP[srcx][srcy].color){
                    return false;
                }
                else{ return true;}
            }  
    }

    @Override
    public void move(ChessPiece[][] CP, int srcx, int srcy, int desx, int desy) {
        super.move(CP, srcx, srcy, desx, desy);
        firstMove = false;
    }
    
    
}
