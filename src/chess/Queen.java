package chess;

public class Queen extends ChessPiece{
    public Queen(String ini, int color)
    {
        this.initial = ini;
        this.color = color;
    }

    @Override
    public boolean isMoveLegal(ChessPiece[][] CP, int srcx, int srcy, int desx, int desy) {
        boolean ans = true;
        if(srcx==desx)
        {
            if(desy > srcy)
            {
                for(int i=srcy+1; i<desy; i++)
                {
                    if(CP[srcx][i]!=null)
                    {
                        return false;
                        
                    }                    
                }
               
            }
            else if(desy < srcy)
            {
                 for(int i=srcy-1; i>desy; i--)
                {
                    if(CP[srcx][i]!=null)
                    {
                        return false;
                    }                    
                }
            }
            else if(desy == srcy)
            {
                return false;
            }
            if(CP[desx][desy]!=null)
            {
                if(CP[desx][desy].color==CP[srcx][srcy].color)
                {
                    return false;
                }
                else{
                    return true;
                }
            }
        }
        else if(srcy==desy)
        {
            if(desx > srcx)
            {
                for(int i=srcx+1; i<desx; i++)
                {
                    if(CP[i][srcy]!=null)
                    {
                        return false;
                    }                    
                }
              
            }
            else if(desx < srcx)
            {
                 for(int i=srcx-1; i>desx; i--)
                {
                    if(CP[i][srcy]!=null)
                    {
                        return false;
                    }                    
                }
            }
            else if(desx == srcx)
            {
                return false;
            }
            if(CP[desx][desy]!=null)
            {
                if(CP[desx][desy].color==CP[srcx][srcy].color)
                {
                    return false;
                }
                else{
                    return true;
                }
            }
        }
        else if(srcx!=desx && srcy!=desy)
        {
            boolean isvalid=false;
            if(desx > srcx && desy > srcy){
                int j=srcy;
                for(int i = srcx; i<=8 && j<=8; i++){
                    if(i == desx && j == desy){
                        isvalid=true;
                        break;
                    }
                    j++;
                }
                if(isvalid)
                {
                    int k = srcy + 1;
                    for(int i = srcx + 1;i <desx && k< desy; i++){
                        if(CP[i][k]!=null){
                            isvalid = false;
                            break;
                        }
                        k++;
                    }
                    if(CP[desx][desy]!=null){if(CP[desx][desy].color==CP[srcx][srcy].color){isvalid = false;}}
                    
                    return isvalid;
                }
                else{return false;}
            }
            else if(desx > srcx && desy < srcy){
                int j=srcy;
                for(int i = srcx; i<=8 && j>=1; i++){
                    if(i == desx && j == desy){
                        isvalid=true;
                        break;
                    }
                    j--;
                }
                if(isvalid)
                {
                    int k = srcy-1;
                    for(int i = srcx+1;i <desx && k > desy; i++){
                        if(CP[i][k]!=null){
                            isvalid = false;
                            break;
                        }
                        k--;
                    }
                    if(CP[desx][desy]!=null){if(CP[desx][desy].color==CP[srcx][srcy].color){isvalid = false;}}
                    
                    return isvalid;
                }
                else{return false;}
            }
            else if(desx < srcx && desy > srcy){
                int j=srcy;
                for(int i = srcx; i>=1 && j<=8; i--){
                    if(i == desx && j == desy){
                        isvalid=true;
                        break;
                    }
                    j++;
                }
                if(isvalid)
                {
                    int k = srcy + 1;
                    for(int i = srcx - 1;i > desx && k< desy; i--){
                        if(CP[i][k]!=null){
                            isvalid = false;
                            break;
                        }
                        k++;
                    }
                    if(CP[desx][desy]!=null){if(CP[desx][desy].color==CP[srcx][srcy].color){isvalid = false;}}
                    
                    return isvalid;
                }
                else{return false;}
            }
            else if(desx < srcx && desy < srcy){
                int j=srcy;
                for(int i = srcx; i>=1 && j>=1; i--){
                    if(i == desx && j == desy){
                        isvalid=true;
                        break;
                    }
                    j--;
                }
                if(isvalid)
                {
                    int k = srcy - 1;
                    for(int i = srcx -1;i >desx && k > desy; i--){
                        if(CP[i][k]!=null){
                            isvalid = false;
                            break;
                        }
                        k--;
                    }
                    if(CP[desx][desy]!=null){if(CP[desx][desy].color==CP[srcx][srcy].color){isvalid = false;}}
                    
                    return isvalid;
                }
                else{return false;}
            }
            
        } 
        return true;
        }
    
}
