package chess;

public class Rook extends ChessPiece{
    public Rook(String ini, int color)
    {
        this.initial = ini;
        this.color = color;
    }

    @Override
    public boolean isMoveLegal(ChessPiece[][] CP, int srcx, int srcy, int desx, int desy) {
        boolean ans = true;
        if(srcx==desx)
        {
            if(desy > srcy)
            {
                for(int i=srcy+1; i<desy; i++)
                {
                    if(CP[srcx][i]!=null)
                    {
                        return false;
                        
                    }                    
                }
               
            }
            else if(desy < srcy)
            {
                 for(int i=srcy-1; i>desy; i--)
                {
                    if(CP[srcx][i]!=null)
                    {
                        return false;
                    }                    
                }
            }
            else if(desy == srcy)
            {
                ans = false;
            }
        }
        else if(srcy==desy)
        {
            if(desx > srcx)
            {
                for(int i=srcx+1; i<desx; i++)
                {
                    if(CP[i][srcy]!=null)
                    {
                        return false;
                    }                    
                }
              
            }
            else if(desx < srcx)
            {
                 for(int i=srcx-1; i>desx; i--)
                {
                    if(CP[i][srcy]!=null)
                    {
                        return false;
                    }                    
                }
            }
        }
        else if(srcx!=desx && srcy!=desy)
        {
            ans = false;
            
        }
        if(CP[desx][desy]!=null)
        {
            if(CP[desx][desy].color==CP[srcx][srcy].color)
            {
                ans = false;
            }
            else{
                ans = true;
            }
        }
        return ans;
    }
    
}
