package chess;
public class Knight extends ChessPiece{
     public Knight(String ini, int color)
    {
        this.initial = ini;
        this.color = color;
    }

    @Override
    public boolean isMoveLegal(ChessPiece[][] CP, int srcx, int srcy, int desx, int desy) {
        boolean isvalid = false;
        if(desx == srcx+1 || desx == srcx-1)
        {
            if(desy == srcy - 2 || desy == srcy + 2)
            {
                isvalid = true;
            }
            else{
                return false;
            }
        }
       else if(desx == srcx+2 || desx == srcx-2)
        {
            if(desy == srcy - 1 || desy == srcy + 1)
            {
                isvalid = true;
            }
            else{
                return false;
            }
        }
        
        if(isvalid)
        {
            if(CP[desx][desy]!= null){
                if(CP[desx][desy].color == CP[srcx][srcy].color){
                    isvalid =  false;
                }
            }
        }
        return isvalid;
    }
     
}
