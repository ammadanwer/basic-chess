package chess;

public class Board {
    public ChessPiece CP[][] = new ChessPiece[9][9];
    public Board()
    {
        for(int i=1; i <=8; i++)
        {
            for(int j=1;j<=8; j++)
            {
                CP[i][j]=null;
            }
        }
        CP[1][1] = new Rook("R1",0);
        CP[1][8] = new Rook("R1",0);
        CP[8][1] = new Rook("R2",1);
        CP[8][8] = new Rook("R2",1);
        CP[1][2] = new Knight("N1",0);
        CP[1][7] = new Knight("N1",0);
        CP[8][2] = new Knight("N2",1);
        CP[8][7] = new Knight("N2",1);
        CP[1][3] = new Bishop("B1",0);
        CP[1][6] = new Bishop("B1",0);
        CP[8][3] = new Bishop("B2",1);
        CP[8][6] = new Bishop("B2",1);
        CP[1][4] = new Queen("Q1",0);
        CP[8][4] = new Queen("Q2",1);
        CP[1][5] = new King("K1",0);
        CP[8][5] = new King("K2",1);
        for(int i=1;i<=8;i++)
        {
            CP[2][i] = new Pawn("P1",0);
        }
        for(int i=0;i<=8;i++)
        {
            CP[7][i] = new Pawn("P2",1);
        }
    }
     public void printBoard()
    {
        System.out.println("+----------------------------------------------------------+");
        System.out.println("||   ||  1 ||  2 ||  3 ||  4 ||  5 ||  6 ||  7 ||  8 ||   ||");
        System.out.println("+----------------------------------------------------------+");
        for(int i=8;i>=1; i--)
        {
            System.out.print("|| " + i + " |");
            for(int j=1; j<=8; j++)
            {
                if(CP[i][j]!=null)
                {
                    System.out.print("| " + CP[i][j].initial+" |");
                }
                else
                {
                    System.out.print("|    |");
                }
            }
            System.out.println("| "+ i + " ||");
        System.out.println("+----------------------------------------------------------+");
        }
        System.out.println("||   ||  1 ||  2 ||  3 ||  4 ||  5 ||  6 ||  7 ||  8 ||   ||");
        System.out.println("+----------------------------------------------------------+");

    }
    
}
