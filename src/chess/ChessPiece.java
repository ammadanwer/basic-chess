package chess;

public abstract class ChessPiece {
    public String initial;      // initials of a piece eg 'K' for king
    public int color;           // 0 means black 1 means white
    
    public void move(ChessPiece CP[][],int srcx, int srcy,int desx, int desy)
    {
        CP[desx][desy]=CP[srcx][srcy];
        CP[srcx][srcy]=null;
    }
    public boolean isMoveLegal(ChessPiece CP[][], int srcx, int srcy,int desx, int desy)
    {
        return true;
    }
}
