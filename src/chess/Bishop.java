package chess;

public class Bishop extends ChessPiece{
    public Bishop(String ini, int color)
    {
        this.initial = ini;
        this.color = color;
    }

    @Override
    public boolean isMoveLegal(ChessPiece[][] CP, int srcx, int srcy, int desx, int desy) {
        if(srcx==desx || srcy==desy)
        {
            return false;
        }
        else{
            boolean isvalid=false;
            if(desx > srcx && desy > srcy){
                int j=srcy;
                for(int i = srcx; i<=8 && j<=8; i++){
                    if(i == desx && j == desy){
                        isvalid=true;
                        break;
                    }
                    j++;
                }
                if(isvalid)
                {
                    int k = srcy + 1;
                    for(int i = srcx + 1;i <desx && k< desy; i++){
                        if(CP[i][k]!=null){
                            isvalid = false;
                            break;
                        }
                        k++;
                    }
                    if(CP[desx][desy]!=null){if(CP[desx][desy].color==CP[srcx][srcy].color){isvalid = false;}}
                    
                    return isvalid;
                }
                else{return false;}
            }
            else if(desx > srcx && desy < srcy){
                int j=srcy;
                for(int i = srcx; i<=8 && j>=1; i++){
                    if(i == desx && j == desy){
                        isvalid=true;
                        break;
                    }
                    j--;
                }
                if(isvalid)
                {
                    int k = srcy-1;
                    for(int i = srcx+1;i <desx && k > desy; i++){
                        if(CP[i][k]!=null){
                            isvalid = false;
                            break;
                        }
                        k--;
                    }
                    if(CP[desx][desy]!=null){if(CP[desx][desy].color==CP[srcx][srcy].color){isvalid = false;}}
                    
                    return isvalid;
                }
                else{return false;}
            }
            else if(desx < srcx && desy > srcy){
                int j=srcy;
                for(int i = srcx; i>=1 && j<=8; i--){
                    if(i == desx && j == desy){
                        isvalid=true;
                        break;
                    }
                    j++;
                }
                if(isvalid)
                {
                    int k = srcy + 1;
                    for(int i = srcx - 1;i > desx && k< desy; i--){
                        if(CP[i][k]!=null){
                            isvalid = false;
                            break;
                        }
                        k++;
                    }
                    if(CP[desx][desy]!=null){if(CP[desx][desy].color==CP[srcx][srcy].color){isvalid = false;}}
                    
                    return isvalid;
                }
                else{return false;}
            }
            else if(desx < srcx && desy < srcy){
                int j=srcy;
                for(int i = srcx; i>=1 && j>=1; i--){
                    if(i == desx && j == desy){
                        isvalid=true;
                        break;
                    }
                    j--;
                }
                if(isvalid)
                {
                    int k = srcy - 1;
                    for(int i = srcx -1;i >desx && k > desy; i--){
                        if(CP[i][k]!=null){
                            isvalid = false;
                            break;
                        }
                        k--;
                    }
                    if(CP[desx][desy]!=null){if(CP[desx][desy].color==CP[srcx][srcy].color){isvalid = false;}}
                    
                    return isvalid;
                }
                else{return false;}
            }
        }
        return false;
    }
}
